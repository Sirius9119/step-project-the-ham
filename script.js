/* Services Tabs*/
let servicesTab = document.querySelectorAll('.services-list-item');
for (let tab of servicesTab) {
    tab.addEventListener('click', () => {
        document.querySelector('.active-services').classList.remove('active-services');
        document.querySelector('.active-content').classList.remove('active-content');
        tab.classList.add('active-services');
        document.querySelector('.services-text[data-services="' + tab.dataset.services + '"]').classList.add('active-content')

    })
}

/*Our Amazing Work*/
const worksTab = document.querySelectorAll('.works-list-item');
const worksItem = document.querySelectorAll('.works-content-item');
const worksItemAct = document.querySelectorAll('.show-item');
const btnLoadMore = document.querySelector('.load-more');

worksTab.forEach((tab) => {
    tab.addEventListener('click', () => {
        document.querySelector('.works-list-active').classList.remove('works-list-active');
        tab.classList.add('works-list-active');
        let tabData = tab.dataset.works;
        worksItem.forEach((item) => {
            item.classList.remove('show-item');
            let itemData = item.dataset.works;
            if (tabData === 'All') {
                if (countBtn !== 2) {
                    btnLoadMore.style.display = 'inline-flex'
                }
                loadMore(worksContentItem);
            } else if (tabData === itemData) {
                btnLoadMore.style.display = 'none'
                item.classList.add('show-item');
            }
        })
    })
})

let worksContentItem = 12;
loadMore(worksContentItem);

function loadMore(n) {
    for (let i = 0; i < n; i++) {
        worksItem[i].classList.add('show-item');
    }
}

let countBtn = 0
btnLoadMore.addEventListener('click', (event) => {
    countBtn++
    if (countBtn === 2) {
        event.target.style.display = 'none'
    }
    let loader = document.querySelector('.loader');
    worksContentItem += 12;
    loader.style.display = 'block'
    setTimeout(() => {
        loadMore(worksContentItem);
        loader.style.display = 'none'
    }, 3000)
})

/*people-reviews*/

let sliderItems = document.querySelectorAll('.slider-item');
let reviews = document.querySelectorAll('.reviews-item');
let current = 0;
for (let i = 0; i < sliderItems.length; i++) {
    sliderItems[i].onclick = function () {
        current = i;
        delClass();
        addClass(current);
    }
}

function addClass(n) {
    reviews[n].classList.add('reviews-active');
    sliderItems[n].classList.add('slider-active');
}

function delClass() {
    document.querySelector('.reviews-active').classList.remove('reviews-active');
    document.querySelector('.slider-active').classList.remove('slider-active');
}

document.querySelector('.next').onclick = function () {
    delClass();
    if (current + 1 === sliderItems.length) {
        current = 0;
    } else {
        current++;
    }
    addClass(current);
}
document.querySelector('.prev').onclick = function () {
    delClass();
    if (current - 1 === -1) {
        current = sliderItems.length - 1;
    } else {
        current--;
    }
    addClass(current);
}